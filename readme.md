ostree
===

## Goals

Initial:
 - Build an ostree composition with CI
 - Tailor the package set to needs
 - Add in extra packages (e.g. systemd-networkd, zfs)

Stretch:
 - utilize package version pinning (similar to CoreOS)
 - signing of ostree composition


## Resources

 - [My Immutable Fedora](https://fale.io/blog/2021/04/29/my-immutable-fedora)
     + [fale-desktop](https://github.com/Fale/fale-desktop)
 - [Pagure: workstation-ostree-config](https://pagure.io/workstation-ostree-config): Documentation for build, hosting, and using.
 - [Pagure: fedora-iot/ostree](https://pagure.io/fedora-iot/ostree)
 - [Gitlab: fedora/ostree](https://gitlab.com/fedora/ostree)
     + [Gitlab: fedora/ostree/ci-test](https://gitlab.com/fedora/ostree/ci-test): doing ostree things in gitlab ci
 - [osbuild](https://www.osbuild.org/): Fedora IoT developer commented on how they are moving to this
